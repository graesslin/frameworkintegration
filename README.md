# Framework Integration

Integration of Qt application with KDE work spaces

## Introduction

Framework Integration is a set of plugins responsible for better integration of
Qt applications when running on a KDE Plasma workspace.

Applications do not need to link to this directly.

## Links

- Home page: <https://projects.kde.org/projects/frameworks/frameworkintegration>
- Mailing list: <https://mail.kde.org/mailman/listinfo/kde-frameworks-devel>
- IRC channel: #kde-devel on Freenode
- Git repository: <https://projects.kde.org/projects/frameworks/frameworkintegration/repository>
